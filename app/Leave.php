<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Employee;

class Leave extends Model
{ 
    protected $fillable=['user_id','from_date','to_date','leave_reason','emergency_contact'];

    public function employees()
    {
    	return $this->HasOne(Employee::class,'user_id','user_id');
    }
}
