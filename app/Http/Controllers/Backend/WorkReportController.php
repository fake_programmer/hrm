<?php

namespace App\Http\Controllers\Backend;

use App\Employee;
use App\Notice;
use App\Projects;
use App\WorkReport;
use Illuminate\Http\Request;
use Session;
use App\Http\Controllers\Controller;

class WorkReportController extends Controller
{


    public function workreport()

    {
        $projectName = Projects::select('name')->get();
        $employee = Employee::select('employee_name')->get();
        $workreport = WorkReport::all();
        return view('Work_Report.view_workreport', compact('workreport', 'projectName', 'employee'));

    }

    public function workreport_create(Request $request)

    {

        $request->validate([
            'project_name' => 'required',
            'description' => 'required',
            'assign_by' => 'required'

        ]);
        $workreport = WorkReport::create([
            'project_name' => $request->project_name,
            'assign_by' => $request->assign_by,
            'description' => $request->description

        ]);
        Session()->flash('message', 'Workreport Submitted Successfully');


        return back();


    }


    Public function workreport_update($id)
    {

        $workreport_info = WorkReport::where('id', $id)->first();
        $workreport_details = WorkReport::all();
        return view('Work_Report.update_workreport', compact('workreport_info', 'workreport_details'));

    }

    Public function updateprocess(Request $request, $id)

    {


        WorkReport::where('id', $id)->update([
            'project_name' => $request->input('project_name'),
            'description' => $request->input('description')

        ]);

//        return back();

//        $workreport=WorkReport::all();
        session()->flash('message', 'WorkReport Update Successfully!!');

        return view('Work_Report.view_workreport', compact('workreport'));


    }


}
