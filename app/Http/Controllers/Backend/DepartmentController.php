<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Department;

class DepartmentController extends Controller
{
    //
     public function create(Request $request)
    {
    	// dd($request->all());
        $this->validate($request,[
            'department_name'=>'required',
            'description'=>'required'
        ]);

    	$dept_name=$request->input('department_name');
        $dept_des=$request->input('description');
    	Department::create([
    			'dept_name'=>$dept_name,
                'dept_des'=>$dept_des,
    	]);
        session()->flash('message','Department Added Successfully!!');

    	return redirect()->back();

    }



     public function update($id)
    {
        $dept_info=Department::where('id',$id)->first();
        $departments=Department::all();


    
 //dd($dept_info);
        return view('emp_management.update_department',compact('dept_info','departments'));
    }
     public function edit(Request $request,$id)
    {
        $this->validate($request,[
            'department name'=>'required',
            'description'=>'required'

        ]);
        Department::where('id',$id)->update([
                'department name'=>$request->input('dept_name'),
                'description'=>$request->input('dept_des'),
        ]);
       
        $all_dept=Department::all();
        return view('emp_management.department',compact('all_dept'));
    }

}
