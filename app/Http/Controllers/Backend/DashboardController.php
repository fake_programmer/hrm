<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Employee;
use App\Attendance;
use App\leave;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;



class DashboardController extends Controller
{
 public function view()
    {
        //dd(Auth::user()->role);
         $today=date('Y-m-d');
    	 $total_employee=Employee::count();
    	 $present=Attendance::whereDate('attendance_date','<=', $today)
            ->whereDate('attendance_date','>=', $today)
            ->count();
    	 // dd($present);
        return view ('dashboard',compact('total_employee','present'));
    }




}