@extends('master')


@section('navbar')
    @include('partials.navbar')
@stop
@section('content')


    <div class="container-fluid" style="padding-top: 5px;">


        @if(Session::has('message'))
            <p class="alert alert-success">{{ Session::get('message') }}</p>
        @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif



        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#designation_modal">
  Add Designation
</button>
        <table class="table table-bordered table-condensed" style="margin-top: 5px;">

            <thead>
            <tr>
                <th> Designation ID</th>
                <th>Designation</th>
                <th>Status</th>
                <th>Action</th>

            </tr>
            </thead>

            <tbody>
            @foreach($all_list as $data)
                <tr>
                    <td>{{$data->id}}</td>
                    <td>{{$data->designation_name}}</td>
                    <td>{{$data->status}}</td>
                   
                    
                    <td>
                        <a class="btn btn-info" href="{{route('update_designation_page',$data->id)}}">Update</a>
                    </td>
            @endforeach


                </tr>
           
            </tbody>
        </table>


    </div>
    <div class="modal fade" id="designation_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Employees Designation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('designation.create')}}" method="POST">
            @csrf()
           <div class="form-group">
            <label for="designation_name">Designation Name</label>
            <input type="text" class="form-control" id="designation_name" placeholder="Enter Designation " name="designation_name">
          </div>
          
        
          </div>
          <button  type="submit" class="btn btn-primary form-control">Submit</button>
        </form>
      </div>
     
    </div>
  </div>
</div>

@stop
