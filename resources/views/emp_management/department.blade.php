@extends('master')
@section('navbar')
    @include('partials.navbar')
@stop
@section('content')


    <div class="container-fluid" style="padding-top: 5px;">

        @if(Session::has('message'))
            <p class="alert alert-success">{{ Session::get('message') }}</p>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#department_modal">
            Add Department
        </button>
        <table class="table table-bordered table-condensed" style="margin-top: 5px;">

            <thead>
            <tr>
                <th>Department ID</th>
                <th>Department Name</th>
                <th>Description</th>
                <th>Staus</th>
                <th>Action</th>

            </tr>
            </thead>

            <tbody>
            @foreach($all_dept as $data)
                <tr>
                    <td>{{$data->id}}</td>
                    <td>{{$data->dept_name}}</td>
                    <td>{{$data->dept_des}}</td>
                    <td>{{$data->status}}</td>

                    <td>
                        <a class="btn btn-info" href="{{route('update_department_page',$data->id)}}">Update</a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>


    </div>



    <!-- Modal -->
    <div class="modal fade" id="department_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Department</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('dept.create')}}" method="POST">
                        @csrf()
                        <div class="form-group">
                            <label for="dept_name">Department Name</label>
                            <input type="text" class="form-control" id="dept_name" placeholder="Enter Department Name"
                                   name="department_name">
                        </div>
                        <div class="form-group">
                            <label for="dept_des">Description</label>
                            <input type="text" class="form-control" id="dept_des" placeholder="Enter Description"
                                   name="description">
                        </div>

                </div>
                <button type="submit" class="btn btn-primary form-control">Submit</button>
                </form>
            </div>

        </div>
    </div>
    </div>
    <!-- modal end -->

@stop

