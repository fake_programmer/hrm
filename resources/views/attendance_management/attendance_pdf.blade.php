




<h1> Employee Attendance List</h1>
 <table class="table table-bordered table-condensed" style="margin-top: 5px; border: 1px solid black;">

            <thead>
            <tr>
                <th>Day</th>
                <th>IN Time</th>
                <th>Out Time</th>
                <th>Present or Absent</th>
                <th>Status</th>
                <th>Late Entry / Right time</th>
                <th>working Hour</th>
            </tr>
            </thead>

            <tbody>
           @if(!empty($attendance))
           @foreach($attendance as $data)
                <tr>
                    <td>{{$data->attendance_date}}</td>
                    <td>{{date('h:i A', strtotime($data->in))}}</td>
                    <td>@if(!empty($data->out)) {{date('h:i A', strtotime($data->out))}} @endif</td>
                    <td>@if(!empty($data->in)) Present @else Absent @endif </td>
                    <td>@if(empty($data->out)) In Office @else Exit @endif</td>
                    <td> @if(strtotime($data->in) > strtotime("09:00:00")) Late Entry @else Right time entry @endif</td>
                    <td>{{floor((strtotime($data->out)-strtotime($data->in))/3600)}} H </td>
                </tr>
                @endforeach
          @endif
            </tbody>
    </table>        
 <div class="row">
 	<div class="col-md-4"></div>
 	<div class="col-md-4"></div>
 	<div class="col-md-2">
 		
 	</div>
 	<div class="col-md-2">
   
 </div>     
 