@extends('master')
 @section('navbar')

    @include('partials.navbar')
@stop

@section('content')

       <div class="row" style="padding: 5px;">
       	<div class="col-md-4"></div>
       	<div class="col-md-4">
       		<button class="btn btn-info in_button @if(!empty($in)) d-none @endif" id="check_in">Check-in</button>
       		<button class="btn btn-success out_button @if(empty($in)) d-none @endif" id="check_out">Check-Out</button>
       	</div>
       	<div class="col-md-4"></div>
       </div>
<div class="row">
	<div class="col-md-6">
        <table class="table table-bordered table-condensed" style="margin-top: 5px;">
            <tbody>
            <tr>
                <th>Name :</th>
                 <td> {{$employee->employee_name}}</td>
             </tr>
             <tr>
              <th>Department :</th>
               <td> {{$employee->dept_name}}</td>
            </tr>
            <tr>
            	<th>Designation :</th>
            	<td> {{$employee->designation_name}}</td>
            </tr>
            </tbody>
           
        </table>
         </div>

</div>
       
        <label for="detail">Attendance Details (This Month)</label>

         <table class="table table-bordered table-condensed" style="margin-top: 5px;">

            <thead>
            <tr>
                <th>Day</th>
                <th>IN Time</th>
                <th>Out Time</th>
                <th>Present or Absent</th>
                <th>Status</th>
                <th>Late Entry / Right time</th>
                <th>working Hour</th>
            </tr>
            </thead>

            <tbody>
           @foreach($attendance as $data)

                @php
                  $workHour = (floor((strtotime($data->out)-strtotime($data->in))/3600)>0)?floor((strtotime($data->out)-strtotime($data->in))/3600):0;
                @endphp
           
                <tr>
                    <td>{{$data->attendance_date}}</td>
                    <td>{{date('h:i A', strtotime($data->in))}}</td>
                    <td>@if(!empty($data->out)) {{date('h:i A', strtotime($data->out))}} @endif</td>
                    <td>@if(!empty($data->in)) Present @else Absent @endif </td>
                    <td> @if(empty($data->out)) In Office @else Exit @endif</td>
                    <td> @if(strtotime($data->in) > strtotime("08:00:00")) Late Entry @else Right time entry @endif</td>
                    <td>{{$workHour}} H </td>
                </tr>
           @endforeach
            </tbody>
        </table>

@stop
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript">

  $(document ).ready(function() {
    $("#check_in").on('click',function(){
    $.ajax({
              // headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
              url:"{{route('attendance.in')}}",
              type:'GET',
              success:function(message){
                window.location.reload();
              }
    })
  });
});


  $(document ).ready(function() {
    $("#check_out").on('click',function(){
    $.ajax({
              // headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
              url:"{{route('attendance.out')}}",
              type:'GET',
              success:function(message){
                window.location.reload();

              }
    })
  });
});
</script>