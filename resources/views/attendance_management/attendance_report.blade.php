@extends('master')
 @section('navbar')

    @include('partials.navbar')
@stop

@section('content')


 @if(Session::has('message'))
 <div class="col-md-12">
<p class="alert alert-info">{{ Session::get('message') }}</p>
</div>
@endif
  <form action="{{route('report.generate')}}" method="POST" role="form">
    <div class="row" style="padding-top: 5px;">
       	@csrf
    	@if(auth()->user()->id==1)

          	
    	 <div class="col-md-4">
       		<label for="select">Select Employees</label>
            <select name="user_id" class="select form-control" required="required">
              <option>--Select Employee--</option>
            	 @foreach($employees as $emp)
              
              <option value="{{$emp->user_id}}">{{$emp->employee_name}}</option>
             @endforeach
            </select>
       	</div>
       	@else

       <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
       @endif

          
       	<div class="col-md-3">
       		<label for="select">Select From Date</label>

           <input value="<?php echo date('Y-m-d'); ?>" type="date" class="form-control" name="from_date" value="bhjg">
       	</div>
       

       
       
       
       	<div class="col-md-3">
       		<label for="select">SelectT To Date</label>
            <input value="<?php echo date('Y-m-d'); ?>" type="date" class="form-control" name="to_date">
       	</div>
       	

       		
       <div class="col-md-2">
       <button  type="submit" style="margin-top: 30px;" class="btn btn-primary form-control"> Generate Report</button>
   		</div>
   	
   	
</div>
</form>

<label for="detail">Attendance Details (This Month)</label>
<div class="col-md-6">
  @if(auth()->user()->id==1)
        <table class="table table-bordered table-condensed" style="margin-top: 5px;">
            <tbody>
            <tr>
                <th>Name :</th>
                 <td> {{$search_emp ? $search_emp->employee_name: ''}}</td>
             </tr>
             
            </tbody>
           
        </table>
@endif
         </div>
        <table class="table table-bordered table-condensed" style="margin-top: 5px;">

            <thead>
            <tr>
                <th>Day</th>
                <th>IN Time</th>
                <th>Out Time</th>
                <th>Present or Absent</th>
                <th>Status</th>
                <th>Late Entry / Right time</th>
                <th>working Hour</th>
            </tr>
            </thead>
            <tbody>
           @if(!empty($attendance))
           @foreach($attendance as $data)
                <tr>
                    <td>{{$data->attendance_date}}</td>
                    <td>{{date('h:i A', strtotime($data->in))}}</td>
                    <td>@if(!empty($data->out)) {{date('h:i A', strtotime($data->out))}} @endif</td>
                    <td>@if(!empty($data->in)) Present @else Absent @endif </td>
                    <td>@if(empty($data->out)) In Office @else Exit @endif</td>
                    <td>@if(strtotime($data->in) > strtotime("09:00:00")) Late Entry @else Right time entry @endif</td>
                    <td>{{floor((strtotime($data->out)-strtotime($data->in))/3600)}} H </td>
                </tr>
                @endforeach
          @endif
          </tbody>
    </table>        
 <div class="row">
 	<div class="col-md-4"></div>
 	<div class="col-md-4"></div>
 	<div class="col-md-2">
 		
 	</div>
 	<div class="col-md-2">
    <a href="{{route('report_pdf')}}">Download As PDF</a>
 </div>      
 


</div>
@stop