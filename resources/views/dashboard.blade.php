@extends('master')


@section('navbar')
    @include('partials.navbar')
@stop


@section('content')

<style type="text/css">
	.hero-widget { text-align: center; padding-top: 20px; padding-bottom: 20px; }
.hero-widget .icon { display: block; font-size: 96px; line-height: 96px; margin-bottom: 10px; text-align: center; }
.hero-widget var { display: block; height: 64px; font-size: 64px; line-height: 64px; font-style: normal; }
.hero-widget label { font-size: 17px; }
.hero-widget .options { margin-top: 10px; }
.col-sm-3{
	background-color: lightgray;
    border: 1px solid lightgray;
	padding: 5px;
    margin: 40px;
}
</style>

<div class="container">
	<div class="row" style="margin-top: 20px;">
		<div class="col-sm-3">
    	    <div class="hero-widget well well-sm">
                <div class="icon">
                     <i class="glyphicon glyphicon-user"></i>
                </div>
                <div class="text">
                	
                    <var>{{$total_employee}}</var>
                    <label class="text-muted">Total Employee</label>
                </div>
              
            </div>
		</div>

        <div class="col-sm-3">
            <div class="hero-widget well well-sm">
                <div class="icon">
                     <i class="glyphicon glyphicon-star"></i>
                </div>
                <div class="text">
                    <var>{{$present}}</var>
                    <label class="text-muted">Total Present</label>
                </div>
                
            </div>
		</div>
	

	

        <div class="col-sm-3">
            <div class="hero-widget well well-sm">
                <div class="icon">
                     <i class="glyphicon glyphicon-tags"></i>
                </div>
                <div class="text" >
                    <var>{{$total_employee-$present}}</var>
                    <label class="text-muted">Total Absent</label>
                </div>
                
            </div>
    	</div>
      
		</div>
	</div>
</div>



@stop