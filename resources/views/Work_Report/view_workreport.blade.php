@extends('master')
@section('navbar')

    @include('partials.navbar')
@stop

@section('content')

    <div class="container-fluid" style="padding-top: 8px;">

      {{dd(auth()->user())}}
        @if(auth()->user()->user_type=='employee')
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newRequest">
                Submit Work Report
            </button>
        @else

        @endif

        <table class="table table-bordered table-condensed" style="margin-top: 5px;">

            <thead>

            <tr>
                <th>Project Name</th>
                <th> Assign By</th>
                <th> Description</th>
                <th>Action</th>


            </tr>
            </thead>

            <tbody>
            @foreach($workreport as $data)
                <tr>


                    <td>{{$data->project_name}}</td>
                    <td>{{$data->assign_by}}</td>
                    <td>{{$data->description}}</td>
                    <td>
                        @if(auth()->user()->id==1)
                            <a class="btn btn-success" href="{{route('workreport_update',$data->id)}}"><i
                                        class="fa fa-edit"></i></a>
                    @endif



{{--                    <!-- <a class="btn btn-info" href="{{route('view_profile',$data->id)}}"><i class="fa fa-eye"></i></a>--}}
{{--                    </td> -->--}}


                </tr>
            @endforeach

            </tbody>
        </table>


        <!-- modal -->
        <div class="modal fade" id="newRequest" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Submit Work Report</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('workreport.create')}}" method="POST" enctype="multipart/form-data"
                              role="form">
                            @csrf()


                            <div class="form-group">
                                <label for="select">Project Name</label>
                                <select class="form-control" id="project_name" name="project_name">
                                    @foreach($projectName as $projectNames)
                                    <option value="{{$projectNames->id}}">{{$projectNames->project_name}}</option>
                                        @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="select">Task Title</label>
                                <input type="text" id="task_title" name="task_title" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="select">Assign By</label>
                                <select class="form-control" id="assign_by" name="assign_by">
                                    @foreach($employee as $employees)
                                        <option>{{$employees->employee_name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="select">Assign Date</label>
                                <input type="Date" id="assign_by" name="assign_date" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="select">Completed Date</label>
                                <input type="Date" id="assign_by" name="completed_date" class="form-control">
                            </div>


                            <div class="form-group">
                                <label for="exampleInputReason"> Description</label>
                                <input name="description" type="text" class="form-control"
                                       id="exampleInputdescription" aria-describedby="description"
                                       placeholder="Enter Description">
                            </div>


                            <button type="submit report" class="btn btn-primary form-control">Submit Report</button>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>



    </div>

@stop