@extends('master')


@section('navbar')

    @include('partials.navbar')
@stop

@section('content')


    <div class="container-fluid" style="padding-top: 8px;">

        @if(Session::has('message'))
            <p class="alert alert-success">{{ Session::get('message') }}</p>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        @if(Auth::user()->user_type=='admin')
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newRequest">
                Create Project
            </button>
        @endif

        <table class="table table-bordered table-condensed" style="margin-top: 5px;">

            <thead>

            <tr>
                <th>Project Name</th>
                <th>Notice Description</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody>
            @foreach($projects as $data)
                <tr>

                    <td>{{$data->name}}</td>
                    <td>{{$data->description}}</td>
                    <td>
                        @if(auth()->user()->id==1)
                            <a href="{{route('project.update',$data->id)}}" class="btn btn-success editbtn" ><i class="fa fa-edit"></i></a>

                    @endif
                </tr>
            @endforeach

            </tbody>
        </table>


                            <!-- modal -->
                                <div class="modal fade" id="newRequest" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Create New Project</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{route('project.create')}}" method="POST"
                                                      enctype="multipart/form-data"
                                                      role="form">
                                                    @csrf()
                                                    <div class="form-group">
                                                        <label for="project"> Project Name</label>
                                                        <input name="name" class="select form-control"
                                                               required="required"
                                                               placeholder="Enter Project Name">


                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputReason">Project Description</label>
                                                        <input name="description" type="text" class="form-control"
                                                               aria-describedby="description"
                                                               placeholder="Enter Description">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputReason">Technical Specification</label>
                                                        <input name="tech_spe" type="text" class="form-control"
                                                               aria-describedby="tech_spe"
                                                               placeholder="Enter Technical Specification">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="select">Client Name</label>
                                                        <select class="form-control" id="client_name"
                                                                name="client_name">
                                                            @foreach($clientName as $clientNames)
                                                                <option value="{{$clientNames->id}}">{{$clientNames->client_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="select">Assign Employee</label>

                                                        <select multiple="multiple" style="width: 200px"
                                                                class="js-example-basic-multiple"
                                                                name="employee_name" id="employee_name"
                                                                multiple="multiple">

                                                            <option value="">Select Employee</option>
                                                            @foreach($employeeName as $employeeNames)
                                                                <option value="{{$employeeNames->id}}">{{$employeeNames->employee_name}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="exampleInputReason">Select Status</label>
                                                        <select class="form-control" id="status" name="status">
                                                            <option>Pending</option>
                                                            <option>Initial</option>
                                                            <option>Development</option>
                                                            <option>Completed</option>
                                                            <option>Support</option>
                                                        </select>
                                                    </div>

                                                    <button type="submit request" class="btn btn-primary form-control">
                                                        Submit Request
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>

                            @stop

