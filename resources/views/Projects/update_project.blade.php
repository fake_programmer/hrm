@extends('master')


@section('navbar')

    @include('partials.navbar')
@stop


@section('content')

    <div class="row">
        @if(Session::has('message'))
            <p class="alert alert-success">{{ Session::get('message') }}</p>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif



        <div class="hero-static offset-md-2 col-md-6 d-flex align-items-bg-white">
            <div class="p-3 w-100">
                <!-- Header -->
                <div class="mb-3 text-center">
                    <p class="text-uppercase font-w700 font-size-sm text-muted">Update Project Information</p>
                </div>

                <form action="{{route('project.updateprocess',$project_info->id)}}" method="POST" role="form"
                      enctype="multipart/form-data">

                    @method('put')
                    @csrf()

                    <div class="form-group">
                        <label for="exampleInputName">Project Name</label>
                        <input name="project_name" type="name" class="form-control" id="exampleInputName"
                               placeholder="edit Name" value="{{$project_info->name}}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputName">Project Description</label>
                        <input name="project_description" type="name" class="form-control" id="exampleInputName"
                               placeholder="edit Description" value="{{$project_info->description}}">
                    </div>


                    <div class="form-group">
                        <label for="exampleInputName">Technical Specification</label>
                        <input name="technical_specification" type="name" class="form-control" id="exampleInputName"
                               placeholder="edit Technical Specification" value="{{$project_info->tec_spe}}">
                    </div>


                    <div class="form-group">
                        <label for="exampleInputName">Status</label>
                        <input name="status" type="name" class="form-control" id="exampleInputName"
                               placeholder="edit Technical Specification" value="{{$project_info->status}}">
                    </div>




                    <button type="submit" class="btn btn-primary form-control">Submit</button>
                </form>
            </div>

        </div>
    </div>


@stop