@extends('master')
@section('navbar')

    @include('partials.navbar')
@stop
@section('content')
<div class="modal-content">
  <div class="content" id="printableArea" name="profile">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="header">
              <h4 class="title" style="margin-left: 20px;">Client Profile</h4>
            </div>
            <div class="row">
            <div class="col-md-3">
                  <img class="avatar border-white" style="height:200px; width:200px;" img src="{{url('/uploads/images/'.$client_info->image)}}">
              </div>
              <div class="col-md-12" >
                <div class="col-md-4">
                  <div class="form-group">
                    <span>Client Name: {{$client_info->client_name}}</span>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <span>Address: {{ $client_info->address }}</span>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <span>Organization Name: {{ $client_info->organization }}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group dept" >
              <label for="dept"> phone number:</label>
             <span>{{$client_info->phone_num}}</span>
              </div>
            </div>
          </div>
            <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="dept">contact person name:</label>
                  <span>{{$client_info->contact_name}}</span>
              </div>
            </div>
          </div>
          </div>
        

        <div class="col-md-12">
          <div class="form-group">
            <label>Project Name: {{$client_info->project_name}}</label>
          </div>
        </div>
</div>
            <div class="col-md-12">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Details: {{$client_info->project_details}}</label>
                 
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                   <span>Assign Date: {{$client_info->assign_date}}</span>
                  <label>Closing Date: {{$client_info->closing_date}}</label>
                  
                </div>
              </div>
            </div> 
        </div>
      </div>
    </div>
    <input type="button" onclick="printDiv('printableArea')" value="print" />
  </div>
</div>
@endsection

<script type="text/javascript">
  function printDiv(profile) {
     var printContents = document.getElementById(profile).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>

































