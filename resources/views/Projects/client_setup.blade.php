@extends('master')
@section('navbar')

    @include('partials.navbar')
@stop

@section('content')


    <div class="container-fluid" style="padding-top: 8px;">

        @if(Session::has('message'))
            <p class="alert alert-success">{{ Session::get('message') }}</p>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


    @if(auth()->user()->id==1)
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newRequest">
                New Client
            </button>
        @endif

        <table class="table table-bordered table-condensed" style="margin-top: 5px;">

            <thead>

            <tr>
                <th>client Name</th>
                <th>Organization</th>
                <th>Client's Phone Number</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody>
            @foreach($clients as $data)
                <tr>

                    <td>{{$data->client_name}}</td>
                    <td>{{$data->organization}}</td>
                    <td>{{$data->phone_num}}</td>
                    <td>
                        @if(auth()->user()->id==1)
                            <a href="{{route('client.update',$data->id)}}" class="btn btn-success editbtn"><i class="fa fa-edit"></i></a>
                            <a class="btn btn-info" href="{{route('view_client_profile',$data->id)}}"><i
                                        class="fa fa-eye"></i></a>
                    @endif
                </tr>
            @endforeach

            </tbody>
        </table>

        <!-- modal -->
        <div class="modal fade" id="newRequest" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"> New Client</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('client.create')}}" method="POST" enctype="multipart/form-data"
                              role="form">
                            @csrf()
                            <div class="form-group">
                                <label for="client_name">Client name</label>
                                <input type="text" name="client_name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="exampleInput">Address</label>
                                <input name="address" type="text" class="form-control" aria-describedby="description"
                                       placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInput">Organization</label>
                                <input name="organization" type="text" class="form-control" placeholder="Organization Info">
                            </div>
                            <div class="form-group">
                                <label for="exampleInput">Phone Number</label>
                                <input name="phone_num" type="number" class="form-control"
                                       aria-describedby="description" placeholder="Please insert your Phone no.">
                            </div>

                            <div class="form-group">
                                <label for="exampleInput">Insert Photo</label>
                                <input type="file"  class="form-control" name="image" accept="image/*" placeholder="Insert Photo"/>
                            </div>



                    </div>
                    <button type="submit request" class="btn btn-primary form-control">Submit Request</button>

                </div>

                </form>

            </div>
=======
<div class="container-fluid" style="padding-top: 8px;">

 @if(auth()->user()->id==1)
 <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newRequest">
  New Client
 </button>
 @endif
 
 <table class="table table-bordered table-condensed" style="margin-top: 5px;">

  <thead>

    <tr>
      <th>client Name</th>
      <th>project name</th>
      <th>project Details</th>
      <th>Action</th>  
    </tr>
  </thead>

  <tbody>
    @foreach($clients as $data)
    <tr>

      <td>{{$data->client_name}}</td>
       <td>{{$data->project_name}}</td>
      <td>{{$data->project_details}}</td>
      <td>
       @if(auth()->user()->id==1)
       <a href="#" class="btn btn-success editbtn"><i class="fa fa-edit"></i></a> 
       <a class="btn btn-info" href="{{route('view_client_profile',$data->id)}}"><i class="fa fa-eye"></i></a>
       @endif
     </tr>
     @endforeach

   </tbody>
 </table>

 <!-- modal -->
 <div class="modal fade" id="newRequest" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> New Client</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('client.create')}}" method="POST" enctype="multipart/form-data" role="form">
          @csrf()
          <div class="form-group">
            <label for="client_name">Client name</label>
            <input type="text" name="client_name" class="form-control" >
          </div>
          <div class="form-group">
          <label for="exampleInputPhoto">Photo</label>
          <input name="image" type="file" class="form-control" id="exampleInputPhoto" placeholder="Upload Photo" required="required">
        </div>
          <div class="form-group">
            <label for="exampleInput">Address</label>
            <input name="address" type="text" class="form-control" aria-describedby="description" placeholder="">
          </div>
           <div class="form-group">
            <label for="exampleInput">Organization</label>
            <input name="organization" type="text" class="form-control" aria-describedby="description" placeholder="">
          </div>
           <div class="form-group">
            <label for="exampleInput">Phone Number</label>
            <input name="phone_num" type="number" class="form-control" aria-describedby="description" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInput">contact person</label>
            <input name="contact_name" type="text" class="form-control" aria-describedby="description" placeholder="">
          </div>
        
          <div class="form-group">
            <label for="exampleInputReason">Project Name</label>
            <input name="project_name" type="text" class="form-control" aria-describedby="description" placeholder="">
          </div>
           <div class="form-group">
            <label for="exampleInputReason">Project Description</label>
            <input name="project_details" type="text" class="form-control" aria-describedby="description" placeholder="Enter Description">
          </div>
          <div class="form-group">
           <label for="select">Select Assigning Date</label>
           <input type="date" id="assign_date" class="form-control" name="assign_date">
          </div>
           <div class="form-group">
           <label for="select">Select closing Date</label>
           <input type="date" id="closing_date" class="form-control" name="closing_date">
          </div>
        <div class="form-group">
         <label for="status">Status</label>
         <select name="status" class="select form-control" required="required">
          <option value="initial">Initial Stage</option>
          <option value="development">development Stage</option>
          <option value="final">final Stage</option>
      
         </select>
       </div>


>>>>>>> 12830a07d9e174d6de209631e915d4cb5a1fe5bd

        </div>

    </div>

@stop

