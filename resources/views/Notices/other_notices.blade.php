@extends('master')
 @section('navbar')

    @include('partials.navbar')
@stop

@section('content')

<div class="container-fluid" style="padding-top: 8px;">
 


     @if(auth()->user()->id==1)
     <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newRequest">
   Create Notice
     </button>
    @endif
 
      <table class="table table-bordered table-condensed" style="margin-top: 5px;">

            <thead>
          
            <tr>
                <th>Notice Title</th>
                <th>Notice Description</th>
                <th>Action</th>
              
                
                
            </tr>
            </thead>

            <tbody>
              @foreach($notices as $data)
                <tr>
                    
                    <td>{{$data->notice_title}}</td>
                    <td>{{$data->notice_description}}</td>
                    <td>
                         @if(auth()->user()->id==1)
                        <a class="btn btn-success" href="{{route('update_notice',$data->id)}}"><i class="fa fa-edit"></i></a>
                        @endif
                       


                       <!-- <a class="btn btn-info" href="{{route('view_profile',$data->id)}}"><i class="fa fa-eye"></i></a>
                    </td> -->
                    
                    
                </tr>
                @endforeach
              
            </tbody>
        </table>


<!-- modal -->
<div class="modal fade" id="newRequest" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create New Notice</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="{{route('notice.create')}}" method="POST" enctype="multipart/form-data" role="form">
            @csrf()
      
          
           <div class="form-group">
          <label for="select">Notice Title</label>
           <input type="text" id="notice_title" name="notice_title" class="form-control" >
          </div>

          


          <div class="form-group">
            <label for="exampleInputReason">Notice Description</label>
            <input name="notice_description" type="text" class="form-control" id="exampleInputdescription" aria-describedby="description" placeholder="Enter Description">
          </div>


        

          </div>
          <button  type="submit request" class="btn btn-primary form-control">Submit Request</button>
        </form>
      </div>



</div>
</div>
</div>
</div>



</div>

@stop