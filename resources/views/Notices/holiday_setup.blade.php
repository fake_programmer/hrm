@extends('master')
 @section('navbar')

    @include('partials.navbar')
@stop

@section('content')

<div class="container-fluid" style="padding-top: 8px;">
  @if(Session::has('message'))
  <p class="alert alert-info">{{ Session::get('message') }}</p>
  @endif


     @if(auth()->user()->id==1)
     <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newRequest">
  Add Holiday
     </button>
    @endif
 
      <table class="table table-bordered table-condensed" style="margin-top: 5px;">

            <thead>
          
            <tr>
                 <th>Holiday Name</th>
                <th>From_date</th>
                <th>To_date</th>
                <th>Duration</th>
                <th>Holiday Description</th>
                
                
            </tr>
            </thead>

            <tbody>
              @foreach($holidays as $data)
                <tr>
                    
                    <td>{{$data->holiday_name}}</td>
                    <td>{{$data->from_date}}</td>
                    <td>{{$data->to_date}}</td>
                    <td>{{(floor((strtotime($data->to_date)-strtotime($data->from_date))/3600/24))}}</td>
                    <td>{{$data->holiday_description}}</td>
                    
                </tr>
                @endforeach
              
            </tbody>
        </table>


<!-- modal -->
<div class="modal fade" id="newRequest" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Holiday</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="{{route('holiday.create')}}" method="POST" enctype="multipart/form-data" role="form">
            @csrf()
      
          
           <div class="form-group">
          <label for="select">Holiday Name</label>
           <input type="text" id="holiday_name" name="holiday_name" class="form-control" >
          </div>

          <div class="form-group">
          <label for="select">Select From Date</label>
           <input type="date" id="from_date" class="form-control" name="from_date">
          </div>

          <div class="form-group">
           <label for="select">Select To Date</label>
           <input type="date" id="to_date" class="form-control" name="to_date">
          </div>


          <div class="form-group">
            <label for="exampleInputReason">Holiday Description</label>
            <input name="holiday_description" type="text" class="form-control" id="exampleInputreason" aria-describedby="description" placeholder="Enter Description">
          </div>


        

          </div>
          <button  type="submit request" class="btn btn-primary form-control">Submit Request</button>
        </form>
      </div>



</div>
</div>
</div>
</div>



</div>

@stop